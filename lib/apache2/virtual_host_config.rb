require_relative 'ssl_config'
require_relative 'config_container'

module Apache2
  class VirtualHostConfig < ConfigContainer
    def initialize ip_address = '*', port = 80, server_name = '', document_root = ''
      super()

      self.ip_address = ip_address
      self.port = port
      self.server_name = server_name
      self.document_root = document_root
    end

    attr_accessor :ip_address, :port

    def server_name
      @directives['ServerName']
    end
    
    def server_name= server_name
      @directives['ServerName'] = server_name
    end

    def document_root
      @directives['DocumentRoot']
    end
    
    def document_root= document_root
      @directives['DocumentRoot'] = document_root
    end

    def server_alias
      @directives['ServerAlias']
    end

    def server_alias= server_alias
      @directives['ServerAlias'] = server_alias
    end
    
    def server_admin
      @directives['ServerAdmin']
    end

    def server_admin= server_admin
      @directives['ServerAdmin'] = server_admin
    end
    
    def to_s spaces=0
      if !self.valid?
        return ''
      end

      if spaces == 0
        new_spaces = 4
      else
        new_spaces = spaces * 2
      end
     
      (' ' * spaces) + "<VirtualHost #{self.ip_address}:#{self.port}>\n" + super(new_spaces) + (' ' * spaces) + '</VirtualHost>'
    end

    def inspect
      print @directives.nil?
    end
    
    def valid?
      !self.ip_address.empty? and !self.port.nil? and !self.server_name.empty? and !self.document_root.empty?
    end
  end
end
