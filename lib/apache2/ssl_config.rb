require_relative 'config'

module Apache2 
  class SslConfig < Config
    def initialize certificate_file = '', certificate_key_file = ''
      super()
      
      @directives['SSLEngine'] = 'on'

      self.certificate_file = certificate_file
      self.certificate_key_file = certificate_key_file
    end

    def certificate_file= certificate_file
      @directives['SSLCertificateFile'] = certificate_file
    end
    
    def certificate_file
      @directives['SSLCertificateFile']
    end

    def certificate_key_file= certificate_key_file
      @directives['SSLCertificateKeyFile'] = certificate_key_file
    end
    
    def certificate_key_file
      @directives['SSLCertificateKeyFile']
    end

    def valid?
      !self.certificate_file.empty? and !self.certificate_key_file.empty?
    end
  end
end
