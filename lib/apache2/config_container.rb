require_relative 'config'

module Apache2
  class ConfigContainer < Config
    def initialize
      super()
    end

    def configs
      @configs
    end
  end
end
