module Apache2
  class Config
    def initialize directives = {}, configs = {}
      @directives = directives
      @configs = configs
    end

    def to_s(spaces=0)
      string = ''

      printed = false
      
      @directives.each do |key, value|
        if value.empty?
          next
        end

        string += (' ' * spaces)
        
        if value.kind_of? Array 
          string += "#{key} #{value.join(' ')}\n"
        else
          string += "#{key} #{value}\n"
        end

        printed = true
      end
      
      @configs.each do |key, value|
        if printed
          string += "\n"
          printed = false
        end

        if value.valid?
          string += value.to_s(spaces)
        end
      end

      return string
    end
  end

  def valid?
    true
  end
end
