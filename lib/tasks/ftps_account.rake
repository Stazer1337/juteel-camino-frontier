namespace :camino do
  namespace :ftps_account do
    desc 'Updates the home directory of each ftps account'
    task update_home_directories: :environment do
      FtpsAccount.includes(:application).each do |ftps_account|
        ftps_account.home_directory = ftps_account.application.ftps_path
        ftps_account.save!
      end
    end
  end
end
