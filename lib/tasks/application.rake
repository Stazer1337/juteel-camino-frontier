namespace :camino do
  namespace :application do
    desc 'Truncates $RAILS_ROOT/applications'
    task cleanup: :environment do
      path = Application.applications_path
      FileUtils.rm_rf(Dir.glob(path.join("*")))  
    end

    desc 'Updates missing subdirectories for each host'
    task update_paths: :environment do
      Application.all.each do |application|
        application.create_paths
      end
    end
  end
end
