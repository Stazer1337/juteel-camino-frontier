module ClientActiveRecord
  class Base < ActiveRecord::Base
    establish_connection "client_#{Rails.env}"
    self.abstract_class = true
  end

  class Migration < ActiveRecord::Migration
    def connection
      ClientActiveRecord::Base.connection
    end
  end
end
