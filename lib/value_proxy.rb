module BitmaskAttributes
  class ValueProxy
    def toggle sym
      code = "@record.#{@attribute}"
      
      if eval("#{code}?(:#{sym})")
        eval("#{code}.delete(:#{sym})")
      else
        eval("#{code} << :#{sym}")
      end
    end
  end
end
