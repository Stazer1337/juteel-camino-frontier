module CaminoActiveRecord
  class Base < ActiveRecord::Base
    establish_connection "camino_#{Rails.env}"
    self.abstract_class = true
  end

  class Migration < ActiveRecord::Migration
    def connection
      CaminoActiveRecord::Base.connection
    end
  end
end
