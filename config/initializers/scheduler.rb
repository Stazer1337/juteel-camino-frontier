require 'rufus-scheduler'

scheduler = Rufus::Scheduler.singleton

Dir[Rails.root.join( 'app', 'crons', '*.rb')].each do |file|
  require file
end
