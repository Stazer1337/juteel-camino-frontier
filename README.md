# README #
juteel-camino repository

### Authors ###
* Justus Flerlage

# Production Deployment #
### Core ###
* Follow instructions at https://bitbucket.org/Stazer1337/juteel-core/wiki/Production%20Deployment for apache2, mariadb and php

### Database ###

* First database server
* * Install dependencies

        `apt-get install mariadb-server`

* *  Create a database

        ``CREATE DATABASE IF NOT EXISTS `juteel_camino`;``

* * Create a database user and grant all privileges on the database that was created before

        `CREATE USER 'juteel_camino_rails'@'%' IDENTIFIED BY 'PASSWORD';`

        ``GRANT ALL PRIVILEGES ON `juteel_camino`.* TO 'juteel_camino_rails'@'%';``

* * Create a database user

        `CREATE USER 'juteel_camino_pure_ftpd'@'%' IDENTIFIED BY 'PASSWORD';`

* * Make sure that the database has been populated by using rake db:migrate
* * Grant privileges on that database

        ``GRANT SELECT ON `juteel_camino`.`ftps_accounts` TO 'juteel_camino_pure_ftpd'@'%';``

* Second database server
* * Follow instructions at https://downloads.mariadb.org/mariadb/repositories/ for installing newest mariadb stable release

* * Create a database user and grant privileges

        `CREATE USER 'juteel_camino'@'%' IDENTIFIED BY 'PASSWORD';`

        ``GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, FILE, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, CREATE USER, EVENT, TRIGGER ON *.* TO 'juteel_camino'@'%' WITH GRANT OPTION;``

### Rails Application ###
* Install dependencies

    `apt-get install git nodejs ruby ruby-rails apache2 libapache2-mod-php5 php5-mysql php5-curl phpmyadmin libapache2-mod-passenger zlib1g-dev libmariadbclient-dev libsqlite3-dev libssl-dev libcrypto++-dev pure-ftpd-mysql`

* Stop apache2 and pure-ftpd daemon 

    `service apache2 stop`

    `service pure-ftpd-mysql stop`

* Disable default site

    `a2dissite 000-default`

* Enable ssl, rewrite and vhost_alias

    `a2enmod ssl rewrite vhost_alias`

* Disable phpmyadmin configuration

    `a2disconf phpmyadmin`

* Clone repository into /var/www/juteel-camino
 
    `git clone -b production git@bitbucket.org:Stazer1337/juteel-camino.git /var/www/juteel-camino`

* Change working directory to /var/www/juteel-camino

    `cd /var/www/juteel-camino`

* Initialize and update submodules

    `git submodule init`

    `git submodule update`

* Install gems

    `bundle install`

* Create needed symlinks for configuration files

    `rm -r /etc/pure-ftpd`

    `ln -s /var/www/juteel-camino/config/apache2.conf /etc/apache2/sites-available/juteel-camino.conf`

    `ln -s /var/www/juteel-camino/config/php.ini /etc/php5/apache2/conf.d/juteel-camino.ini`

    `ln -s /var/www/juteel-camino/config/pure-ftpd /etc/pure-ftpd`

* Edit configuration files
* Enable juteel-camino site

    `a2enssite juteel-camino`

* Precompile assets

    `rake assets:precompile RAILS_ENV=production`

* Migrate database

    `rake db:migrate RAILS_ENV=production`

* Change permissions

    `chown -R www-data:www-data ./`

    `chmod -R 700 ./`

* Start apache2 and pure-ftpd daemon 

    `service apache2 start`

    `service pure-ftpd-mysql start`

# Production Upgrade #
## General ##
* Pull repository changes

    `git pull origin production`

* If given follow specific upgrade instructions

* Install gems

    `bundle install`

* Migrate database

    `rake db:migrate RAILS_ENV=production`

* Precompile assets

    `rake assets:precompile RAILS_ENV=production`

* Change permissions

    `chown -R www-data:www-data ./`

    `chmod -R 700 ./`

* Restart daemons

    `service apache2 restart`

    `service pure-ftpd restart`

## 1.0.0 to 2.0.0 ##
* docker...