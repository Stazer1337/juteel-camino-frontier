require 'unix_crypt'

class FtpsAccount < CaminoActiveRecord::Base
  belongs_to :application
  has_many :ftps_account_logins, dependent: :destroy
  
  validates :application,
            presence: true
  
  validates :username,
            uniqueness: true,
            presence: true,
            length: {in: 3..32},
            format: {with: /\A[A-Za-z0-9\_\-]*\z/}

  validates :password,
            presence: {on: :create},
            confirmation: true,
            length: {minimum: 4, allow_nil: true}
  
  validates :password_confirmation,
            presence: true, if: '!password.nil?'

  attr_accessor :password, :password_confirmation

  attr_readonly :username
  
  before_validation on: :create do
    self.username = "#{self.application.name}_#{self.username}"
  end
  
  before_create do
    self.home_directory = self.application.ftps_path
  end
  
  before_save do
    if self.password.present?
      self.password_hash = UnixCrypt::DES.build password 
      
      self.password = self.password_confirmation = nil
    end
  end
  
  def to_param
    username
  end
end
