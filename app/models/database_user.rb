class DatabaseUser < CaminoActiveRecord::Base
  validates :application,
            presence: true

  validates :username,
            uniqueness: {case_sensitive: false}

  validates :password,
            confirmation: true,
            length: {minimum: 4, allow_nil: true}

  validates :password_confirmation,
            presence: true, if: '!password.nil?'
  
  belongs_to :application
  has_many :databases, dependent: :destroy

  attr_accessor :password, :password_confirmation
  
  attr_readonly :username
  
  def hostname
    '%'
  end

  before_validation on: :create do
    self.username = self.application.name
  end
  
  before_create do
    ClientActiveRecord::Base.connection.execute "CREATE USER IF NOT EXISTS '#{self.username}'@'#{self.hostname}' IDENTIFIED BY '" + Array.new(24){[*'A'..'Z', *'0'..'9', *'a'..'z'].sample}.join + "';"
  end

  before_save do
    if self.password.present?
      ClientActiveRecord::Base.connection.execute "SET PASSWORD FOR '#{self.username}'@'#{self.hostname}' = PASSWORD('#{self.password}');"
      
      self.password = self.password_confirmation = nil
    end
  end
  
  def to_param
    username
  end
end
