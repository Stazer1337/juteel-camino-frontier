require 'docker'
require 'ipaddr'

class Container < CaminoActiveRecord::Base
  validates :name,
            uniqueness: {case_sensitive: false},
            presence: true
              
  validates :application,
            presence: true
  
  belongs_to :application
  
  # Checks if the container is running
  def running?
    handle.json['State']['Running']
  end

  # Checks if the container has stopped
  def stopped?
    !self.running?
  end

  # Returns information about the container as json encoded data
  def information
    handle.json
  end

  # Returns the ip address of the container
  def ip_address
    IPAddr.new handle.json['NetworkSettings']['IPAddress']
  end

  # Returns the ip address of the gateway
  def gateway
    IPAddr.new handle.json['NetworkSettings']['Gateway']
  end
  
  ['start', 'stop', 'kill', 'restart'].each do |name|
    define_method name do
      handle.send name
    end
  end
  
  before_validation do
    self.name = "#{self.application.name}_#{SecureRandom.hex(4)}"
  end
  
  before_create do
    if !Docker::Image::exist? "juteel-camino-php"
      throw "Docker image does not exist!"
    end

    Docker::Container.create(name: self.name, image: 'juteel-camino-php', hostconfig: {binds: ["#{self.application.public_path}:/var/www"]})
  end

  after_destroy do
    if self.running?
      self.kill
    end

    handle.delete
  end

  # Returns the docker-api handle
  private def handle
    @handle = Docker::Container.get self.name if (defined? @handle).nil?
    @handle
  end
end
