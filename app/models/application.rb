# coding: utf-8
require 'fileutils'

class Application < CaminoActiveRecord::Base
  validates :account_id,
            presence: true
  
  validates :name,
            uniqueness: {case_sensitive: false},
            presence: true,
            length: {in: 3..15},
            format: {with: /\A[A-Za-z0-9\-]*\z/},
            exclusion: {within: %w(admin root mysql phpmyadmin test camino juteel), case_sensitive: false}

  has_one :database_user, dependent: :destroy
  has_many :container, dependent: :destroy
  has_many :ftps_accounts, dependent: :destroy
  has_many :domains#, dependent: :destroy; take a look at before_destroy
  has_many :ssl_certificates, dependent: :destroy

  attr_readonly :name
  
  def to_param
    name
  end

  # Returns the $RAILS_ROOT/applications path
  def self.applications_path
    Rails.root.join 'applications'
  end

  # Returns the root path of the application
  def root_path
    Application.applications_path.join self.name
  end

  PATHS = ['public', 'log', 'tmp', 'certs', 'config', 'ftps']
  
  PATHS.each do |method|
    define_method "#{method}_path" do
      self.root_path.join method
    end
  end

  # Returns the url suffix
  def self.url_suffix
    '.camino.juteel.com'
  end

  # Returns the url of the application
  def url
    self.name + Application.url_suffix
  end

  # Creates the root path and all its needed subdirectory paths
  def create_paths
    PATHS.each do |name|
      root_path = self.root_path
      
      ([root_path] + PATHS).each do |path|
        new_path = root_path.join path
        Dir.mkdir new_path unless Dir.exists? new_path
      end
    end
  end
 
  before_validation on: :create do
    self.name = self.name.downcase
  end

  before_create do
    self.create_paths    
  end
  
  after_create do
    DatabaseUser.create application_id: self.id

    self.domains.create name: self.url, flags: [:primary, :enabled, :http]

    Container.create application_id: self.id
    self.container.first.start
  end
  
  before_destroy do
    domain = self.domains.first  
    domain.flags.delete :primary
    domain.save! 

    self.domains.destroy_all

    FileUtils.rm_rf self.root_path
  end
end
