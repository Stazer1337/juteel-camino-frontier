require 'resolv'

class Domain < CaminoActiveRecord::Base
  belongs_to :application
  belongs_to :ssl_certificate
  
  validates :application,
            presence: true
  
  validates :name,
            uniqueness: {case_sensitive: false},
            presence: true,
            length: {in: 3..32},
            format: {with: /\A[A-Za-z0-9\.\-]*\z/}

  bitmask :flags, as: [:primary, :enabled, :http, :https]

  attr_readonly :name

  before_validation on: :create do
    self.name = self.name.downcase
  end
  
  validate do
    # TODO: add error messages for the case that the user tries to enable http or https while domain is disabled
    if self.ssl_certificate_id != nil and self.ssl_certificate_id_changed? and !self.application.ssl_certificates.include? self.ssl_certificate
      self.errors.add :ssl_certificate. I18t.t('activerecord.errors.messages.ssl_certificate_not_part_of_application')
    end
    
    if self.flags?(:primary)
      if !self.flags?(:enabled)
        self.errors.add :flags, I18n.t('activerecord.errors.messages.primary_cannot_be_disabled')
      end

      if !self.flags?(:http)
        self.errors.add :flags, I18n.t('activerecord.errors.messages.primary_http_cannot_be_disabled')
      end
    end
  end

  before_save do
    if !self.flags? :enabled
      self.flags.delete :http
      self.flags.delete :https
    end

    if !self.flags? :https
      self.ssl_certificate = nil
    end
  end

  before_destroy do
    if self.flags? :primary
      self.errors.add :flags, I18n.t('activerecord.errors.messages.primary_cannot_be_destroyed')
      false
    end
  end
  
  def to_param
    name
  end

  def http?
    self.flags?(:enabled, :http)
  end
  
  def https?
    self.flags?(:enabled, :https) && self.ssl_certificate
  end
  
  def addresses
    Resolv.getaddresses self.name
  end

  def whois
    Whois.whois(self.name)
  end

  def urls
    urls = []

    if self.flags? :http
      urls.push "http://#{self.name}"
    end
    if self.flags? :https
      urls.push "https://#{self.name}"
    end

    return urls
  end
end
