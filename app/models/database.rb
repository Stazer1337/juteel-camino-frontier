class Database < CaminoActiveRecord::Base
  validates :database_user,
            presence: true
  
  validates :name,
            uniqueness: {case_sensitive: false},
            presence: true,
            length: {in: 1..24},
            format: {with: /\A[A-Za-z0-9_-]*\z/}

  belongs_to :database_user
  
  attr_readonly :name

  before_validation on: :create do
    self.name = "#{self.database_user.name}_#{self.name.downcase}"
  end

  before_create do
    self.grant_privileges
  end

  after_destroy do
    self.revoke_privileges
  end

  def to_param
    name
  end

  public def grant_privileges
    query = "START TRANSACTION;
CREATE DATABASE IF NOT EXISTS #{self.name};
GRANT ALL ON #{self.name}.* TO '#{self.database_user.username}'@'#{self.database_user.hostname}';
COMMIT;"
    
    ClientActiveRecord::Base.connection.execute query
  end
  
  public def revoke_privileges
    query = "START TRANSACTION;
SET FOREIGN_KEY_CHECKS = 0;
REVOKE ALL ON #{self.name}.* FROM '#{self.database_user.username}'@'#{self.database_user.hostname}';
DROP DATABASE IF EXISTS #{self.name};
SET FOREIGN_KEY_CHECKS = 1;
COMMIT;"
    
    ClientActiveRecord::Base.connection.execute query
  end
end
