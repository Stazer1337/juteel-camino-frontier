class FtpsAccountLogin < CaminoActiveRecord::Base
  belongs_to :ftps_account

  validates :ftps_account,
            presence: true
  
  validates :address,
            presence: true

  # TODO
end
