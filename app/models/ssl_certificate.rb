class SslCertificate < CaminoActiveRecord::Base
  belongs_to :application

  validates :application,
            presence: true
  
  validates :name,
            uniqueness: {case_sensitive: false},
            presence: true,
            length: {in: 3..32},
            format: {with: /\A[A-Za-z0-9\_\-]*\z/}

  validates :public_key, presence: true, on: :create
  validates :private_key, presence: true, on: :create

  validate on: :create do
    begin
      certificate = OpenSSL::X509::Certificate.new self.public_key
      
      begin
        private_key = OpenSSL::PKey::RSA.new self.private_key
        if !certificate.verify private_key
          raise ""
        end
      rescue
        self.errors.add :private_key, :invalid
      end
    rescue
      self.errors.add :public_key, :invalid
      self.errors.add :private_key, :invalid
    end
  end
  
  attr_readonly :name
  attr_accessor :public_key, :private_key

  def to_param
    name
  end
  
  # Returns the path of the public key
  def public_key_path
    self.application.certs_path + "#{self.name}.crt" 
  end

  # Returns the path of the private key
  def private_key_path
    self.application.certs_path + "#{self.name}.key" 
  end

  before_validation on: :create do
    self.name = "#{self.application.name}_#{self.name.downcase}"
  end

  before_create do
    File.open(self.public_key_path, "w") do |f|
      f.chmod(0444)
      f.write(self.public_key)
    end

    File.open(self.private_key_path, "w") do |f|
      f.chmod(0400)
      f.write(self.private_key)
    end
  end

  after_destroy do
    FileUtils.rm self.public_key_path if File.exist? self.public_key_path
    FileUtils.rm self.private_key_path if File.exist? self.private_key_path
  end
end
