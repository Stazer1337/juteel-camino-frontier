module ApplicationHelper
  def require_application
    @application = Application.find_by name: (params[:application_id].nil? ? params[:id] : params[:application_id]), account_id: session_manager.account.id
 
    if @application.nil?
      redirect_to root_path
    end
  end

  def require_ftps_account
    @ftps_account = FtpsAccount.find_by username: (params[:ftps_account_id].nil? ? params[:id] : params[:ftps_account_id]), application_id: @application.id
  end

  def require_domain
    @domain = Domain.find_by name: (params[:domain_id].nil? ? params[:id] : params[:domain_id]), application_id: @application.id
    
    if @domain.nil?
      redirect_to applications_path
    end
  end

  def require_ssl_certificate
    @ssl_certificate = SslCertificate.find_by name: (params[:ssl_certificate_id].nil? ? params[:id] : params[:ssl_certificate_id]), application_id: @application.id
    
    if @ssl_certificate.nil?
      redirect_to applications_path
    end
  end
  
  def require_database_user
    @database_user = @application.database_user
  end

  def require_database
    @database = Database.find_by name: (params[:database_id].nil? ? params[:id] : params[:database_id]), database_user_id: @application.database_user.id

    if @database.nil?
      redirect_to applications_path
    end
  end
end
