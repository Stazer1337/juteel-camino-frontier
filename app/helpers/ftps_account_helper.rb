module FtpsAccountHelper
  def last_login ftps_account
    last = ftps_account.ftps_account_logins.last
    return I18n.l(last.created_at) unless last.nil?
    t('never')
  end
end
