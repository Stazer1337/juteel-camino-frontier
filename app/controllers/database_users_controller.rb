class DatabaseUsersController < ApplicationController
  before_action :require_application
  before_action :require_database_user

  def show
  end
  
  def edit
  end

  def update
    if @database_user.update_attributes params.require(:database_user).permit(:password, :password_confirmation)
      @alerts = {t('entity_update_successfull' , entity: DatabaseUser.model_name.human) => :success}
      render 'show' 
    else
      @errors = @database_user.errors
      render 'edit'
    end
  end
end
