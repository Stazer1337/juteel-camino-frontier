class ContainersController < ApplicationController
  before_action :require_application

  def index
    @containers = Container.where application_id: @application.id
  end
end
