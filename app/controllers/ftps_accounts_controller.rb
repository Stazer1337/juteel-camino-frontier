class FtpsAccountsController < ApplicationController
  before_action :require_application
  before_action :require_ftps_account, only: [:edit, :update, :destroy]
  
  def index
    @ftps_accounts = FtpsAccount.where application_id: @application.id
  end

  def new
  end
  
  def create
    @ftps_account = FtpsAccount.new params.require(:ftps_account).permit(:username, :password, :password_confirmation).merge(application_id: @application.id)

    if @ftps_account.save
      @alerts = {t('entity_creation_successfull', entity: FtpsAccount.model_name.human) => :success}
      index
      render 'index'
    else
      @errors = @ftps_account.errors
      render 'new'
    end
  end

  def edit
  end

  def update
    if @ftps_account.update_attributes params.require(:ftps_account).permit(:password, :password_confirmation)
      @alerts = {t('entity_update_successfull', entity: FtpsAccount.model_name.human) => :success}
      index
      render 'index'
    else
      @errors = @ftps_account.errors
      render 'edit'
    end
  end
  
  def destroy
    @ftps_account.destroy

    @alerts = {t('entity_deletion_successfull', entity: FtpsAccount.model_name.human) => :success}
    index
    render 'index'
  end
end
