class SslCertificatesController < ApplicationController
  before_action :require_application
  before_action :require_ssl_certificate, only: [:show, :destroy]
  
  def index
    @ssl_certificates = SslCertificate.where application_id: @application.id
  end
  
  def show
  end
  
  def new
  end
  
  def create
    @ssl_certificate = SslCertificate.new params.require(:ssl_certificate).permit(:name, :public_key, :private_key).merge(application_id: @application.id)

    if @ssl_certificate.save
      @alerts = {t('entity_creation_successfull', entity: SslCertificate.model_name.human) => :success}
      index
      render 'index'
    else
      @errors = @ssl_certificate.errors
      render 'new'
    end
  end

  def destroy
    @ssl_certificate.destroy!

    index
    render 'index'
  end
end
