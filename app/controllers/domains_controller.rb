# coding: utf-8
class DomainsController < ApplicationController
  before_action :require_application
  before_action :require_domain, only: [:show, :destroy, :edit, :update]
  
  def index
    @domains = Domain.where application_id: @application.id
  end

  def new
  end

  def show
  end

  def edit
  end
  
  def update
    if params.include? :flags and params.permit(flags: ['enable', 'http', 'https'])
      @domain.flags.toggle(params[:flags])
      if !@domain.save
        @alerts = @domain.errors
      else
        @alerts = {t('entity_update_successfull', entity: Domain.model_name.human) => :success}
      end
    elsif params.require(:domain).permit(:ssl_certificate)
      @domain.ssl_certificate = @application.ssl_certificates.find_by_name params[:domain][:ssl_certificate]

      if !@domain.save!
        @alerts = @domain.errors
      else
        @alerts = {t('entity_update_successfull', entity: Domain.model_name.human) => :success}  
        params.clear
      end
    end

    render 'edit'
  end
  
  def create
    @domain = Domain.new params.require(:domain).permit(:name).merge(application_id: @application.id)
                                                                     
    if @domain.save
      @alerts = {t('entity_creation_successfull', entity: Domain.model_name.human) => :success}
      index
      render 'index'
    else
      @alerts = @domain.errors
      render 'new'
    end
  end

  def destroy
    if @domain.destroy
      @alerts = {t('entity_deletion_successfull', entity: Domain.model_name.human) => :success}
    else
      @alerts = {@domain.errors[:flags][0] => :danger}
    end

    index
    render 'index'
  end
end
