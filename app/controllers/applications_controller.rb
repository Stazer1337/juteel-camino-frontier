class ApplicationsController < ApplicationController
  before_action :require_application, only: [:show, :destroy]
  
  def index
    @applications = Application.includes(:domains).where account_id: session_manager.account.id
  end

  def show
  end
  
  def new
  end

  def create
    @application = Application.new params.require(:application).permit(:name).merge(account_id: session_manager.account.id)

    if @application.save
      @alerts = {t('entity_creation_successfull', entity: Application.model_name.human) => :success}
      index
      render "index"
    else
      @errors = @application.errors
      render "new"
    end
  end

  def destroy
    @application.destroy!

    @alerts = {t('entity_deletion_successfull', entity: Application.model_name.human) => :success}

    self.index
    render 'index'
  end
end
