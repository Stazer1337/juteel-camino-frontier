class DatabasesController < ApplicationController
  before_action :require_application
  before_action :require_database, only: [:destroy]
  
  def index
    @databases = Database.where database_user_id: @application.database_user.id
  end

  def new
  end
  
  def create
    @database = Database.new params.require(:database).permit(:name).merge(database_user_id: @application.database_user.id)
                                                                     
    if @database.save
      @alerts = {t('entity_creation_successfull', entity: Database.model_name.human) => :success}
      index
      render 'index'
    else
      @errors = @database.errors
      render 'new'
    end
  end

  def destroy
    @database.destroy

    @alerts = {t('entity_deletion_successfull', entity: Database.model_name.human) => :success}
    index
    render 'index'
  end
end
