class CreateSslCertificates < CaminoActiveRecord::Migration
  def change
    create_table :ssl_certificates do |t|
      t.integer :application_id, null: false

      t.string :name, null: false

      t.timestamp null: false
    end
  end
end
