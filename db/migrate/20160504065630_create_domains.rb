class CreateDomains < CaminoActiveRecord::Migration
  def change
    create_table :domains do |t|
      t.integer :host_id, null: false

      t.string :name, null: false

      t.timestamps null: false
    end
  end
end
