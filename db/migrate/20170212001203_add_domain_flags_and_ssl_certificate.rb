class AddDomainFlagsAndSslCertificate < CaminoActiveRecord::Migration
  def change
    add_column :domains, :ssl_certificate_id, :integer, null: true
    add_column :domains, :flags, :integer, null: false, default: 0
    
    Application.all.each do |application|
      application.domains.create name: host.url, flags: [:primary]
    end
  end
end
