class CreateHosts < CaminoActiveRecord::Migration
  def change
    create_table :hosts do |t|
      t.integer :account_id, null: false      

      t.string :name, null: false
      t.integer :disk_usage, null: false, default: 0, limit: 8
      
      t.timestamps null: false
    end
  end
end
