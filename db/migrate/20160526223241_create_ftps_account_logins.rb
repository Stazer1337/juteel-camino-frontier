class CreateFtpsAccountLogins < CaminoActiveRecord::Migration
  def change
    create_table :ftps_account_logins do |t|
      t.integer :ftps_account_id, null: false

      t.string :address, null: false
      
      t.timestamps null: false
    end
  end
end
