class CreateDatabases < CaminoActiveRecord::Migration
  def change
    create_table :databases do |t|
      t.integer :database_user_id, null: false

      t.string :name, null: false

      t.timestamps null: false
    end
  end
end
