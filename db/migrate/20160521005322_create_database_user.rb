class CreateDatabaseUser < CaminoActiveRecord::Migration
  def change
    create_table :database_users do |t|
      t.integer :host_id, null: false

      t.string :username, null: false

      t.timestamp null: false
    end
  end
end
