class CreateFtpsAccounts < CaminoActiveRecord::Migration
  def change
    create_table :ftps_accounts do |t|
      t.integer :host_id, null: false

      t.string :username, null: false
      t.string :password_hash, null: false

      t.string :home_directory, null: false
      
      t.timestamps null: false
    end
  end
end
