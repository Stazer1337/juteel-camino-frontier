class RenameHostsToApplications < CaminoActiveRecord::Migration
  def change
    rename_table :hosts, :applications

    rename_column :database_users, :host_id, :application_id
    rename_column :domains, :host_id, :application_id
    rename_column :ftps_accounts, :host_id, :application_id
  end
end
